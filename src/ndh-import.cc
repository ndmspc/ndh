#include <getopt.h>
#include <iostream>
#include <yaml-cpp/yaml.h>
#include <TFile.h>
#include <TTree.h>

#include "HnSparse.hh"
#include "ndh.hh"

void version()
{
    Printf("%s v%d.%d.%d-%s", NDH_NAME, NDH_VERSION_MAJOR(NDH_VERSION), NDH_VERSION_MINOR(NDH_VERSION),
           NDH_VERSION_PATCH(NDH_VERSION), NDH_VERSION_RELEASE);
}

[[noreturn]] void help(int rc = 0)
{
    version();
    Printf("");
    Printf("Usage: [OPTION]...");
    Printf("");
    Printf("Options:");
    Printf("       -c, --config[=VALUE]          ndh config file");
    Printf("");
    Printf("       -h, --help                    display this help and exit");
    Printf("       -v, --version                 output version information and exit");
    Printf("");
    Printf("Examples:");
    Printf("       %s-import -c ndh.yaml", NDH_NAME);
    Printf("                                     Using ndh.yaml config file");
    Printf("");
    Printf("Report bugs at <https://gitlab.com/ndmspc/ndh>");
    Printf("General help using GNU software: <https://www.gnu.org/gethelp/>");

    exit(rc);
}

int main(int argc, char ** argv)
{

    // ***** Default values START *****
    /// Config file
    std::string        configFile     = "";
    std::string        filename       = "";
    std::string        objname        = "";
    std::vector<Int_t> axisSplit      = {};
    std::string        outputFilename = "";

    YAML::Node config;
    config["input"]["filename"] =
        "root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_pp_AOD/"
        "987/phi_leading_3s/AnalysisResults.root";
    config["input"]["objname"]   = "Unlike";
    config["split"]["axes"]      = YAML::Load("[1, 2]");
    config["output"]["filename"] = "/tmp/ndh.root";

    // ***** Default values END *****

    std::string   shortOpts  = "hvc:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"config", required_argument, nullptr, 'c'},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v':
            version();
            exit(0);
            break;
        case 'c': configFile = optarg; break;
        default: help(1);
        }
    } while (nextOption != -1);

    version();

    if (!configFile.empty()) {
        try {
            Printf("Loading config file '%s' ...", configFile.data());
            config = YAML::LoadFile(configFile);
        }
        catch (YAML::BadFile ex) {
            Printf("Error: Cannot open config file '%s' !!!", configFile.data());
            return 1;
        }
        catch (const YAML::ParserException & ex) {
            Printf("Errorl loading yaml file %s", ex.what());
            return 2;
        }
    }

    std::cout << config << std::endl;

    filename       = config["input"]["filename"].as<std::string>();
    objname        = config["input"]["objname"].as<std::string>();
    axisSplit      = config["split"]["axes"].as<std::vector<int>>();
    outputFilename = config["output"]["filename"].as<std::string>();

    NDH::HnSparseL h;
    h.SetOutputFileName(outputFilename.data());
    h.Import(axisSplit, filename.data(), objname.data());

    TFile *     f  = TFile::Open(outputFilename.data());
    TTree *     t  = (TTree *)f->Get("ndh");
    THnSparse * ss = nullptr;
    t->SetBranchAddress("h", &ss);

    for (Int_t i = 0; i < t->GetEntries(); i++) {
        t->GetEntry(i);
        ss->Print();
    }

    t->GetUserInfo()->Print();

    return 0;
}
