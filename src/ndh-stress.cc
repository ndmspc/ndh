#include <TFile.h>
#include <TH2.h>
#include "HnSparse.hh"
#include "HnSparseStress.hh"
#include "ndh.hh"

int main(int argc, char ** argv)
{
    Printf("%s v%d.%d.%d-%s", NDH_NAME, NDH_VERSION_MAJOR(NDH_VERSION), NDH_VERSION_MINOR(NDH_VERSION),
           NDH_VERSION_PATCH(NDH_VERSION), NDH_VERSION_RELEASE);

    Long64_t    size        = 1e5;
    bool        bytes       = false;
    std::string filename    = "/tmp/HnSparse.root";
    bool        projections = false;

    int      nDim      = 10;
    int      nBins     = 1000;
    double   min       = -nBins / 2;
    double   max       = nBins / 2;
    Long64_t chunkSize = 1024 * 1024;

    Int_t    bins[nDim];
    Double_t mins[nDim];
    Double_t maxs[nDim];
    for (Int_t i = 0; i < nDim; i++) {
        bins[i] = nBins;
        mins[i] = min;
        maxs[i] = max;
    }

    NDH::HnSparseD * h = new NDH::HnSparseD("hNStress", "hNStress", nDim, bins, mins, maxs, chunkSize);

    NDH::HnSparseStress stress;

    TStopwatch timeStress;
    timeStress.Start();
    stress.Stress(h, size, bytes);
    timeStress.Stop();
    timeStress.Print("m");
    h->Print();
    Long64_t nBinsSizeBytes = sizeof(Double_t) * h->GetNbins();
    Printf("size : %03.2f MB (%lld B)", (double)nBinsSizeBytes / (1024 * 1024), nBinsSizeBytes);

    TStopwatch timeWrite;
    timeWrite.Start();
    TFile * f = TFile::Open(filename.data(), "RECREATE");
    f->SetCompressionSettings(ROOT::RCompressionSetting::EDefaults::kUseAnalysis);
    Printf("Writing histogram ...");
    h->Write();
    if (projections) {
        Printf("Creating and writing projections ...");
        for (Int_t i = 0; i < nDim; i++) {
            ((TH1 *)h->Projection(i))->Write();
            for (Int_t j = 0; j < nDim; j++) {
                if (i == j) continue;
                ((TH2 *)h->Projection(j, i))->Write();
            }
        }
    }
    f->Close();
    timeWrite.Stop();
    timeWrite.Print("m");
    Printf("Output was saved in '%s' size : %.2f MB (%lld B)", filename.data(),
           (double)f->GetBytesWritten() / (1024 * 1024), f->GetBytesWritten());

    return 0;
}
