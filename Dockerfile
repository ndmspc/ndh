FROM registry.gitlab.com/ndmspc/ndhep/dep/c7 AS builder
RUN yum install -y rpm-build
RUN yum update -y
RUN yum clean all
COPY . /builder/
WORKDIR /builder
RUN scripts/make.sh rpm

FROM centos:7
COPY --from=builder /builder/build/RPMS/x86_64/*.rpm /
RUN yum install -y epel-release yum-plugin-copr
RUN yum copr enable ndmspc/stable  -y
RUN yum install -y *.rpm
ENTRYPOINT [ "ndhep-test" ]
